Source: mitmproxy
Section: net
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Andrej Shadura <andrewsh@debian.org>
Build-Depends: dh-python, python3, python3-setuptools,
 pandoc,
 python3-asgiref (>= 3.2.10),
 python3-blinker (>= 1.4),
 python3-brotli (>= 1.0),
 python3-certifi (>= 2019.9.11),
 python3-click (>= 7.0),
 python3-cryptography (>= 3.2),
 python3-coverage <!nocheck>,
 python3-flask (>= 1.1.1),
 python3-h2 (>= 4.0),
 python3-hypothesis (>= 5.8) <!nocheck>,
 python3-hyperframe (>= 6.0),
 python3-kaitaistruct (>= 0.7),
 python3-ldap3 (>= 2.8),
 python3-msgpack (>= 1.0.0),
 python3-openssl (>= 19.1.0),
 python3-protobuf (>= 3.6.0),
 python3-passlib (>= 1.6.5),
 python3-pyasn1 (>= 0.3.1),
 python3-pyparsing (>= 2.4.2),
 python3-pyperclip (>= 1.6.0),
 python3-pytest <!nocheck>,
 python3-pytest-cov <!nocheck>,
 python3-pytest-timeout <!nocheck>,
 python3-pytest-asyncio <!nocheck>,
 python3-publicsuffix2 (>= 2.20190812),
 python3-requests (>= 2.9.1),
 python3-ruamel.yaml (>= 0.16),
 python3-sortedcontainers (>= 2.1.0),
 python3-tornado (>= 4.3),
 python3-urwid (>= 2.1.0),
 python3-wsproto (>= 0.14),
 tox (>= 3.5) <!nocheck>,
 locales-all <!nocheck>,
 python3-zstandard,
 debhelper-compat (= 13)
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/debian/mitmproxy.git
Vcs-Browser: https://salsa.debian.org/debian/mitmproxy
Homepage: https://mitmproxy.org

Package: mitmproxy
Architecture: all
Pre-Depends:
 dpkg (>= 1.17.14),
Depends:
 fonts-font-awesome (>= 4.2.0~dfsg),
 python3-h2 (>= 4.0),
 python3-hyperframe (>= 6.0),
 python3-pkg-resources,
 ${misc:Depends},
 ${python3:Depends},
Conflicts:
 python-netlib,
Description: SSL-capable man-in-the-middle HTTP proxy
 mitmproxy is an SSL-capable man-in-the-middle HTTP proxy. It provides
 a console interface that allows traffic flows to be inspected and
 edited on the fly.
 .
 Also shipped is mitmdump, the command-line version of mitmproxy, with
 the same functionality but without the frills. Think tcpdump for
 HTTP.
 .
 Features:
  - intercept and modify HTTP traffic on the fly
  - save HTTP conversations for later replay and analysis
  - replay both HTTP clients and servers
  - make scripted changes to HTTP traffic using Python
  - SSL interception certs generated on the fly
